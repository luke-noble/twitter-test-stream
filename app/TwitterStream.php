<?php

namespace App;

use OauthPhirehose;
use App\SearchWord;
use App\Jobs\ProcessTweet;
use Illuminate\Foundation\Bus\DispatchesJobs;

class TwitterStream extends OauthPhirehose
{
    use DispatchesJobs;

    /**
    * Enqueue each status
    *
    * @param string $status
    */
    public function enqueueStatus($status)
    {
        $this->dispatch(new ProcessTweet($status));
    }

    public function checkFilterPredicates()
    {
        $trackIds = SearchWord::pluck('search_word')->toArray();
        if ($trackIds != $this->getTrack()) {
            $this->setTrack($trackIds);
        }
    }
}