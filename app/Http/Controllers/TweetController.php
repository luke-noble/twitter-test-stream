<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SearchWord;
use App\Tweet;
class TweetController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    

    public function show()
    {
    	$searchWords = SearchWord::all();
    	return view('search-words', compact('searchWords'));
    }

    public function store()
    {
        $this->validate(request(), [
            'search_word' => 'required',
            'real_or_fake' => 'required'
            ]);
        SearchWord::create([
            'search_word' => request('search_word'),
            'count' => 0,
            'real_or_fake' => request('real_or_fake'),
            ]);

        return redirect('/search-words');
    }

    public function destroy($id)
    {
        $searchWord = SearchWord::find($id);
        $searchWord->delete();
        return Redirect('/search-words');
    }

    public function deletedata()
    {
        $tweets = Tweet::pluck('search_word')->toArray();
        $countedTweets = array_count_values($tweets);
        foreach( $countedTweets as $searchWordID => $count)
        {
            if($searchWordID != 0){
                $tempName = SearchWord::withTrashed()->find($searchWordID);
                $tempName->count = $tempName->count + $count;
                $tempName->save();
            }
        }
        Tweet::truncate();
        return Redirect('/search-words');
    }
}
