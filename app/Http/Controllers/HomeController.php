<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SearchWord;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $searchWordFake = SearchWord::withTrashed()
        ->inRandomOrder()
        ->where('real_or_fake', 'fake')
        ->take(1)
        ->get();
        $searchWordReal = SearchWord::withTrashed()
        ->inRandomOrder()
        ->where('real_or_fake', 'real')
        ->take(1)
        ->get();
        return view('welcome', compact('searchWordFake', 'searchWordReal'));
    }
}
