<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SearchWord extends Model
{
	use SoftDeletes;
    protected $fillable = ['search_word', 'count', 'real_or_fake'];

    protected $dates = ['deleted_at'];
}
