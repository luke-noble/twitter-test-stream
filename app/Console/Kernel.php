<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Tweet;
use App\SearchWord;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\ConnectToStreamingAPI::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $tweets = Tweet::pluck('search_word')->toArray();
            $countedTweets = array_count_values($tweets);
            foreach( $countedTweets as $searchWordID => $count)
            {
                if($searchWordID != 0){
                    $tempName = SearchWord::withTrashed()->where('id', $searchWordID)->get();
                    $tempName = $tempName[0];
                    $tempName->count = $tempName->count + $count;
                    $tempName->save();
                }
            }
            Tweet::truncate();
        })->everyMinute();

        $schedule->call(function() {
            $searchWords = SearchWord::all();
            foreach ( $searchWords as $searchWord) {
                $searchWord->count = 0;
                $searchWord->last_reset = date('Y-m-d H:i:s');
                $searchWord->save();
            }
        })->dailyAt('00:00');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
