<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Real Vs Fake News') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
	<div class="wrapper">
		<div class="left-box">
            <div class="title">
                <span class="fake">FAKE</span><span class="news">NEWS</span>
            </div>
            <div class="underline-fake"></div>
			<div class="search-word">{{ $searchWordFake['0']->search_word}}</div>
            <div id="fake-news-circle"></div>
			<div class="counter">
                <div class="fake">{{ $searchWordFake['0']->count }}<br><span class="mentions">MENTIONS</span>
            </div>
            </div>
		</div>

		<div class="right-box">
            <div class="title">
                <span class="real">REAL</span><span class="news">NEWS</span>
                <div class="underline-real"></div>
            </div>
            
		    <div class="search-word">{{ $searchWordReal['0']->search_word}}</div>
            <div id="real-news-circle"></div>
		    <div class="counter">
                <div class="real">{{ $searchWordReal['0']->count }}<br><span class="mentions">MENTIONS</span>
            </div>
            </div>
		</div>
	</div>
    <div class="vs-description">
        <div class="vs">Vs</div>
        <div class="ring-description">1 ring = 50,000 mentions since midnight</div>
    </div>
    <div class="footer">
    FAKE NEWS VS REAL NEWS is the brain child of Zoe Coultan and Luke Noble. <a href="http://zoecoultan.co.uk">http://zoecoultan.co.uk</a> <a href="https://luke-noble.co.uk">https://luke-noble.co.uk</a><br>
    Copyright Zoe Coultan and Luke Noble 2017
    </div>

    <!-- Scripts -->
    <script>
    var number = {{ $searchWordFake['0']->count }} / 50000 ;
    var content = '';
    var div = document.createElement('div');
    div.className = 'circle';
    for (var i = 0; i < number; i++) {
        content += '<span style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); width: calc(100% - ' + i + '0px); height: calc(100% - ' + i + '0px); border: 1px solid #DD4D57; border-radius: 50%;"></span>';
        div.innerHTML = '<div style="position: relative; width: 100%; height: 100%; margin: 0 auto;">' + content + '</div>';
        document.getElementById('fake-news-circle').appendChild(div);
    }
    </script>
    <script>
    var number = {{ $searchWordReal['0']->count }} / 50000 ;
    var content = '';
    var div = document.createElement('div');
    div.className = 'circle';
    for (var i = 0; i < number; i++) {
        content += '<span style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); width: calc(100% - ' + i + '0px); height: calc(100% - ' + i + '0px); border: 1px solid #3DAE46; border-radius: 50%;"></span>';
        div.innerHTML = '<div style="position: relative; width: 100%; height: 100%; margin: 0 auto;">' + content + '</div>';
        document.getElementById('real-news-circle').appendChild(div);
    }
    </script>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>