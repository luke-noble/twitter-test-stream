@extends('layouts.app')

@section('content')
  <div class="container">
      <div class="row">
          <div class="col-md-8 col-md-offset-2">
              <div class="panel panel-default">
                  <div class="panel-heading">Search Words</div>
                  <div class="panel-body">
<table class="table table-striped">
	<tr>
		<th>Search Word</th>
		<th>Real or Fake</th>
		<th></th>
	</tr>
	@foreach ( $searchWords as $searchWord)
		<tr>
			<td>{{ $searchWord->search_word }}</td>
			<td>{{ $searchWord->real_or_fake }}</td>
			<td>
				<form method="POST" action="/search-words/{{$searchWord->id}}" class="pull-right">
					{{csrf_field()}}
					<input name="_method" type="hidden" value="DELETE">
                    <input class="btn btn-danger" type="submit" value="Delete this search word?">
                </form>
			</td>
		</tr>
	@endforeach
</table>

<form method="POST" action="/search-words">
{{ csrf_field() }}
  		<div class="form-group">
    		<label for="title">Add New Search Word</label>
    		<input type="text" class="form-control" id="title" name="search_word">
  		</div>
  		<div class="form-group">
  			<label for="real_or_fake">Real or Fake?</label>
  			<select name="real_or_fake">
  				<option value="real">real</option>
  				<option value="fake">fake</option>
  			</select>
  		</div>
      <div class="form-group">
  		  <button type="submit" class="btn btn-primary">Add search term</button>
      </div>

</form>
<form method="POST" action="/reset-data">
{{ csrf_field() }}
      <div class="form-group">
  		  	<button type="submit" class="btn btn-danger">Reset data</button>
      </div>

</form>
</div>
</div>
</div>
</div>
</div>

@endsection