<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index');
Route::get('/search-words', 'TweetController@show');
Route::post('/search-words', 'TweetController@store');
Route::delete('/search-words/{id}', 'TweetController@destroy');
Route::post('/reset-data', 'TweetController@deletedata');
Auth::routes();

